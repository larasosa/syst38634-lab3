package vowels;


import java.util.Scanner;

public class vowels {

	
	public static void main(String args[]){
	      int count = 0;
	      System.out.println("Enter something with vowels :\n");
	      Scanner scanner = new Scanner(System.in);
	      String vowels = scanner.nextLine();

	      for (int i=0 ; i<vowels.length(); i++){
	         char ch = vowels.charAt(i);
	         if(ch == 'a'|| ch == 'e'|| ch == 'i' ||ch == 'o' ||ch == 'u'||ch == ' '){
	            count ++;
	         }
	      }
	      System.out.println("Vowel count is \n"+count);
	   }
	
	
}
