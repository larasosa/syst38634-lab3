package middle;

import java.util.Scanner;


public class middle {

	public static void main(String[] args)
    {
        Scanner num = new Scanner(System.in);
        System.out.print("Enter a string: \n");
        
        String string = num.nextLine();
        System.out.print("Middle char is: " + middle(string)+"!");
    }
 public static String middle(String str)
    {
        int loc;
        int len;
        if (str.length() % 2 ==  0)
        {
            loc = str.length() / 2 - 1;
            len = 2;
        }
        else
        {
            loc =  str.length() / 2;
            len = 1;
        }
        return str.substring(loc, loc + len);
    }
 
 
 
 
}
