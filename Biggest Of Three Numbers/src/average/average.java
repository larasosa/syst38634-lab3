package average;

import java.util.Scanner;


public class average {
	public static void main(String[] args)
    {
        Scanner num = new Scanner(System.in);
        System.out.print("First: \n");
        double a = num.nextDouble();
        System.out.print("Second: \n");
        double b = num.nextDouble();
        System.out.print("Third: \n");
        double c = num.nextDouble();
        System.out.print("Average " + average(a, b, c)+"\n" );
    }

  public static double average(double a, double b, double c)
    {
        return (a + b + c) / 3;
    }
}
