package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	/*
	 * @BeforeClass public static void setUpBeforeClass() throws Exception { }
	 * 
	 * @AfterClass public static void tearDownAfterClass() throws Exception { }
	 * 
	 * @Before public void setUp() throws Exception { }
	 * 
	 * @After public void tearDown() throws Exception { }
	 */

	@Test
	public void testCheckPasswordLengthHappy() {
		boolean password = PasswordValidator.checkPasswordLength("123456789");
		System.out.println(password);
		assertTrue("password was wrong",password==true);
	}
	@Test 
	public void testCheckPasswordLengthSad() {
		boolean password = PasswordValidator.checkPasswordLength("qwer");
		System.out.println(password);
		assertTrue("password is too short, ", password== false);
	}
	@Test 
	public void testCheckPasswordLengthBoundaryIn() {
		boolean password = PasswordValidator.checkPasswordLength("12345678");
		System.out.println(password);
		assertTrue("password is the exact amount needed", password== true);
	}
	@Test 
	public void testCheckPasswordLengthBoudaryOut() {
		boolean password = PasswordValidator.checkPasswordLength("1234567");
		assertTrue("password is right under the amount necessary", password==false);
	}	
	
	
	@Test 
	public void testCheckPasswordDigitsHappy() {
		boolean password = PasswordValidator.checkDigits("qwer12345");
		assertTrue("password has enough numbers", password==true);
	}
	@Test 
	public void testCheckPasswordDigitsSad() {
		boolean password = PasswordValidator.checkDigits("qweriuqwert");
		assertTrue("password needs more digits", password==true);
	}
	@Test 
	public void testCheckPasswordDigitsBoundaryIn() {
		boolean password = PasswordValidator.checkDigits("qwutruytrer12");
		assertTrue("password is ok for digits", password==true);
	}
	@Test 
	public void testCheckPasswordDigitsBoundaryOut() {
		boolean password = PasswordValidator.checkDigits("qwdgfdgfer1");
		assertTrue("password password is weak on digits", password==true);
	}

	/*@Test
	public void testCheckDigits() {
		fail("Not yet implemented");
	}*/

}
